package com.learning.fooddataviewer

import com.learning.fooddataviewer.di.DaggerTestComponent
import com.learning.fooddataviewer.model.database.ProductDao
import com.learning.fooddataviewer.model.dto.ProductDto
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

class AndroidTestApplication : App() {

    val productDtosSubject = PublishSubject.create<List<ProductDto>>()
    private val productDao: ProductDao = object : ProductDao() {

        override fun get(): Observable<List<ProductDto>> {
            return productDtosSubject
        }

        override fun getProduct(barcode: String): Single<ProductDto> {
            throw NotImplementedError("Not implemented in instrumented testing")
        }

        override fun insert(productDto: ProductDto): Completable {
            throw NotImplementedError("Not implemented in instrumented testing")
        }

        override fun delete(barcode: String): Completable {
            throw NotImplementedError("Not implemented in instrumented testing")
        }

    }

    override val component by lazy {
        DaggerTestComponent.builder()
            .context(this)
            .productDao(productDao)
            .build()
    }
}
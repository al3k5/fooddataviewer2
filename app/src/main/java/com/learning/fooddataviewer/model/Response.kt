package com.learning.fooddataviewer.model

import com.learning.fooddataviewer.model.dto.ProductDto
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Response (
    val product: ProductDto
)
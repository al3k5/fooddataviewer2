package com.learning.fooddataviewer.scan.handlers

import com.learning.fooddataviewer.model.ProductRepository
import com.learning.fooddataviewer.scan.BarcodeError
import com.learning.fooddataviewer.scan.ProcessBarcode
import com.learning.fooddataviewer.scan.ProductLoaded
import com.learning.fooddataviewer.scan.ScanEvent
import com.learning.fooddataviewer.utils.IdlingResource
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProcessBarcodeHandler @Inject constructor(
    private val productRepository: ProductRepository,
    private val idlingResource: IdlingResource
) :
    ObservableTransformer<ProcessBarcode, ScanEvent> {
    override fun apply(upstream: Observable<ProcessBarcode>): ObservableSource<ScanEvent> {
        return upstream.switchMap { effect ->
            productRepository.getProductFromApi(effect.barcode)
                .map { product ->
                    idlingResource.decrement()
                    ProductLoaded(product) as ScanEvent
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { error ->
                    idlingResource.decrement()
                }
                .onErrorReturnItem(BarcodeError)
                .toObservable()
        }
    }
}
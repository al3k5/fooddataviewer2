package com.learning.fooddataviewer.fooddetails

import com.learning.fooddataviewer.model.Product

data class FoodDetailsModel(
    val activity: Boolean = false,
    val product: Product? = null,
    val error: Boolean = false
)
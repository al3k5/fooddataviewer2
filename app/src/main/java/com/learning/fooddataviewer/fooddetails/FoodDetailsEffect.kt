package com.learning.fooddataviewer.fooddetails

import com.learning.fooddataviewer.model.Product

sealed class FoodDetailsEffect

data class LoadProdcut(val barcode: String) : FoodDetailsEffect()

data class DeleteProduct(val barcode: String) : FoodDetailsEffect()

data class SaveProduct(val product: Product) : FoodDetailsEffect()
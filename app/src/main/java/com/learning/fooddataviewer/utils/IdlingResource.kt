package com.learning.fooddataviewer.utils

interface IdlingResource {

    fun increment()

    fun decrement()
}
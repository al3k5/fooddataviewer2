package com.learning.fooddataviewer.foodlist

import com.learning.fooddataviewer.model.Product

data class FoodListModel(
    val products: List<Product> = listOf()
)